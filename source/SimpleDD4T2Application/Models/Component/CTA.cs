﻿using DD4T.Core.Contracts.ViewModels;
using DD4T.Mvc.ViewModels.Attributes;
using DD4T.ViewModels.Attributes;
using DD4T.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SimpleDD4T2Application.Models.Component
{
    [ContentModel("cta", true)]
    public class CTA : ViewModelBase, IRenderableViewModel
    {
        [TextField]
        public string Heading { get; set; }

        [RenderData]
        public IRenderData RenderData { get; set; }
    }
}